import { createStackNavigator } from 'react-navigation-stack';
import RestuarantsScreens from "../screens/Restaurants";

const RestaurantsScreensStacks = createStackNavigator({
    Restaurants: {
        screen: RestuarantsScreens,
        navigationOptions: ()=>({
            title: "Restaurantes"            
        })
    }
});

export default RestaurantsScreensStacks;