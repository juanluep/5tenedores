import React from 'react';
import { Icon } from 'react-native-elements';
import { createAppContainer } from 'react-navigation';
import { createBottomTabNavigator } from 'react-navigation-tabs';

import RestaurantsScreensStacks from './RestaurantsStacks';
import TopListScreensStacks from './TopListsStacks';
import SearchScreensStacks from './SearchStacks';
import MyAccountScreensStacks from './MyAccountStacks';

const NavigationStacks = createBottomTabNavigator({
  Restaurants: {
    screen: RestaurantsScreensStacks,
    navigationOptions: () => ({
      tabBarLabel: 'Restaurantes',
      tabBarIcon: ({ tintColor }) => (
        <Icon
          type="material-community"
          name="compass-outline"
          size={22}
          color={tintColor}
        />
      )
    })
  },
  TopList: {
    screen: TopListScreensStacks,
    navigationOptions: () => ({
      tabBarLabel: 'Ranking',
      tabBarIcon: ({ tintColor }) => (
        <Icon
          type="material-community"
          name="star-outline"
          size={22}
          color={tintColor}
        />
      )
    })
  },
  Search: {
    screen: SearchScreensStacks,
    navigationOptions: () => ({
      tabBarLabel: 'Buscar',
      tabBarIcon: ({ tintColor }) => (
        <Icon
          type="material-community"
          name="magnify"
          size={22}
          color={tintColor}
        />
      )
    })
  },
  Account: {
    screen: MyAccountScreensStacks,
    navigationOptions: () => ({
      tabBarLabel: 'Cuenta',
      tabBarIcon: ({ tintColor }) => (
        <Icon
          type="material-community"
          name="home-outline"
          size={22}
          color={tintColor}
        />
      )
    })
  },
},
{
    initialRouteName: "Account",
    order: ["Restaurants", "TopList", "Search", "Account"],
    tabBarOptions: {
        inactiveTintColor: "#646464",
        activeTintColor: "#00a680"
    }
}
);
export default createAppContainer(NavigationStacks);
