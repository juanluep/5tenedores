import { createStackNavigator } from 'react-navigation-stack';
import MyAccountScreens from '../screens/Account/MyAccount';
import LoginScreen from '../screens/Account/Login';
import RegisterScreen from '../screens/Account/Register';

const MyAccountScreensStacks = createStackNavigator({
  MyAccount: {
    screen: MyAccountScreens,
    navigationOptions: () => ({
      title: 'Mi perfil'
    })
  },
  Login: {
    screen: LoginScreen,
    navigationOptions: () => ({
      title: 'Login'
    })
  },
  Register: {
    screen: RegisterScreen,
    navigationOptions: () => ({
      title: 'Registro'
    })
  }
});

export default MyAccountScreensStacks;
