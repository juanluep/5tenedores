import { createStackNavigator } from 'react-navigation-stack';
import TopRestuarantsScreens from "../screens/TopRestaurants";

const TopListScreensStacks = createStackNavigator({
    TopRestaurants: {
        screen: TopRestuarantsScreens,
        navigationOptions: ()=>({
            title: "Los mejores restaurantes"            
        })
    }
});

export default TopListScreensStacks;