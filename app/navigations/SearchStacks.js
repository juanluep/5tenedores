import { createStackNavigator } from 'react-navigation-stack';
import SearchScreens from "../screens/Search";

const SearchScreensStacks = createStackNavigator({
    Search: {
        screen: SearchScreens,
        navigationOptions: ()=>({
            title: "Busca tu restaurante"            
        })
    }
});

export default SearchScreensStacks;