import React from 'react';
import { StyleSheet, View, Text } from 'react-native';
import { Avatar } from 'react-native-elements';
import * as firebase from 'firebase';
import * as Permissions from 'expo-permissions';
import * as ImagePicker from 'expo-image-picker';

export default function InfoUser(props) {
    const {
        userInfo: { uid, displayName, email, photoURL }
    } = props;

    const changeAvatar = async () => {
        const resultPermissions = await Permissions.askAsync(Permissions.CAMERA_ROLL);
        const resultPermissionsCamera = resultPermissions.permissions.cameraRoll.status;

        if (resultPermissionsCamera === 'denied') {
            console.log('sin permisos')
        } else {
            const result = await ImagePicker.launchImageLibraryAsync({
                allowsEditing: true,
                aspect: [4, 3]
            });

            if (result.cancelled) {
                console.log('Has cancelado la carga de imagnes');
            } else {
                uploadImage(result.uri, uid).then(() => {
                    console.log('Upload ok');
                });
            }
        }
    };

    const uploadImage = async (uri, name) => {
        const response = await fetch(uri);
        const blob = await response.blob();
        const ref = firebase.storage().ref().child(`avatar/${name}`);
        return ref.put(blob);
        console.log('URI: ' + uri + ' name: ' + name);

    }

    return (
        <View style={styles.viewUserInfo}>
            <Avatar
                rounded
                size="large"
                showEditButton
                onEditPress={changeAvatar}
                containerStyle={styles.userInfoAvatar}
                source={{
                    uri: photoURL ? photoURL : 'https://api.adorable.io/avatars/266/abott@adorable.png'
                }}
            />
            <View>
                <Text style={styles.displayName}>
                    {displayName ? displayName : 'Anonimo'}
                </Text>
                {email ? <Text>{email}</Text> : null}
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    viewUserInfo: {
        alignItems: 'center',
        justifyContent: 'center',
        flexDirection: 'row',
        backgroundColor: '#F2F2',
        paddingTop: 30,
        paddingBottom: 30
    },
    userInfoAvatar: {
        marginRight: 20
    },
    displayName: {
        fontWeight: 'bold'
    }
})