import React, { useState } from 'react';
import { SocialIcon } from 'react-native-elements';
import * as firebase from 'firebase';
import * as Facebook from 'expo-facebook';
import { FacebookApi } from '../../utils/Social';
import Loading from '../Loading';



export default function LoginFacebook(props) {
    const { toastRef, navigation } = props;
    const [isVisible, setIsVisible] = useState(false);
    const login = async () => {
        await Facebook.initializeAsync(FacebookApi.application_id);

        const { type, token } = await Facebook.logInWithReadPermissionsAsync(
            { permissions: FacebookApi.permissions }
        );



        if (type === 'success') {
            setIsVisible(true);
            const credentials = firebase.auth.FacebookAuthProvider.credential(token);
            await firebase.auth()
                .signInWithCredential(credentials)
                .then(() => {
                    navigation.navigate('MyAccount');
                })
                .catch(err => {
                    toastRef.current.show('Error en el registro de Facebook ' + err);
                });
        } else if (type === 'cancel') {
            toastRef.current.show('Ha cancelado el acceso con Facebook ');
        } else {
            toastRef.current.show('Error en el registro de Facebook ' + type);
        }
        setIsVisible(false);
    };
    return (
        <>
            <SocialIcon
                title="Inciar sesion Facebook"
                button
                type="facebook"
                onPress={login}
            />

            <Loading isVisible={isVisible} text="Iniciando Sesión" />
        </>
    )
}