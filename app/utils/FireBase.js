import firebase from "firebase/app";

  const firebaseConfig = {
    apiKey: "AIzaSyCWv8f2bcTJBeuOry7E5GNDtxA3G7N6BT4",
    authDomain: "tenedores-2fedf.firebaseapp.com",
    databaseURL: "https://tenedores-2fedf.firebaseio.com",
    projectId: "tenedores-2fedf",
    storageBucket: "tenedores-2fedf.appspot.com",
    messagingSenderId: "742515502022",
    appId: "1:742515502022:web:c44ecb936671ea7629e0c7"
  };

  export const firebaseApp = firebase.initializeApp(firebaseConfig);